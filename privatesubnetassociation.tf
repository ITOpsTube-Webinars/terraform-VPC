resource "aws_route_table_association" "wp_private1_assoc" {
  subnet_id      = "${aws_subnet.wp_private1_subnet.id}"
  route_table_id = "${aws_default_route_table.wp_private_rt.id}"
}

resource "aws_route_table_association" "wp_private2_assoc" {
  subnet_id      = "${aws_subnet.wp_private2_subnet.id}"
  route_table_id = "${aws_default_route_table.wp_private_rt.id}"
}

resource "aws_route_table_association" "wp_private3_assoc" {
  subnet_id      = "${aws_subnet.wp_private3_subnet.id}"
  route_table_id = "${aws_default_route_table.wp_private_rt.id}"
}

resource "aws_route_table_association" "wp_private4_assoc" {
  subnet_id      = "${aws_subnet.wp_private4_subnet.id}"
  route_table_id = "${aws_default_route_table.wp_private_rt.id}"
}

resource "aws_route_table_association" "wp_private5_assoc" {
  subnet_id      = "${aws_subnet.wp_private5_subnet.id}"
  route_table_id = "${aws_default_route_table.wp_private_rt.id}"
}

resource "aws_route_table_association" "wp_private6_assoc" {
  subnet_id      = "${aws_subnet.wp_private6_subnet.id}"
  route_table_id = "${aws_default_route_table.wp_private_rt.id}"
}
