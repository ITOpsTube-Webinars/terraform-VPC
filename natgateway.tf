resource "aws_eip" "neweip" {
  vpc   = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id = "${element(aws_eip.neweip.*.id, count.index)}"
  subnet_id     = "${element(aws_subnet.wp_public1_subnet.*.id, count.index)}"
}

resource "aws_route" "nat_gateway" {
  route_table_id         = "${element(aws_default_route_table.wp_private_rt.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.nat.*.id, count.index)}"

}
